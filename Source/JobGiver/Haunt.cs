﻿using Merthsoft.Hauntings.GameComponent;
using Merthsoft.Hauntings.Pawn;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;
using Verse.AI;

namespace Merthsoft.Hauntings.JobGiver {
    public class Haunt : JobGiver_Wander {
        protected override IntVec3 GetWanderRoot(Verse.Pawn pawn) {
            if (pawn.Map.IsAlmostDaytime()) {
                return (pawn as GhostPawn)?.GraveLocation ?? pawn.Position;
            } else {
                var deathLocation = (pawn as GhostPawn)?.DeathLocation;
                if (deathLocation == null) {
                    deathLocation = pawn.Position;
                }
                for (int i = 0; i < 5; i++) {
                    deathLocation = deathLocation.Value.RandomAdjacentCell8Way();
                }
                return deathLocation.Value;
            }
        }
    }
}
