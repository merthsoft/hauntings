﻿using Merthsoft.Hauntings.Pawn;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace Merthsoft.Hauntings.GameComponent {
    public class HauntingComponent : MapComponent {
        public int HauntingHour = 3;
        private Dictionary<string, IntVec3> DeathLocations;
        readonly List<GhostPawn> ActiveGhosts;
        public void SetPawnDeathLocation(Verse.Pawn pawn)
            => DeathLocations[pawn.ThingID] = pawn.Position;

        public IntVec3? GetPawnDeathLocation(Verse.Pawn pawn)
            => DeathLocations.ContainsKey(pawn.ThingID) ? DeathLocations[pawn.ThingID] : (IntVec3?)null;

        public HauntingComponent(Map map) : base(map) {
            DeathLocations = new Dictionary<string, IntVec3>();
            ActiveGhosts = new List<GhostPawn>();
        }

        public override void MapComponentTick() {
            if (GenLocalDate.HourFloat(map) == HauntingHour) {
                ActiveGhosts.Clear();
                var allGraves = Find.CurrentMap.listerBuildings.AllBuildingsColonistOfClass<Building_Grave>();
                allGraves.ForEach((graveNumber, grave) => {
                    if (grave.HasAnyContents && grave.ContainedThing is Corpse corpse) {
                        var pawn = corpse.InnerPawn;
                        var ghost = PawnGenerator.GeneratePawn(new PawnGenerationRequest(Hauntings.GhostPawnKind, forceGenerateNewPawn: true,
                            fixedBiologicalAge: pawn.ageTracker.AgeBiologicalYearsFloat, fixedChronologicalAge: pawn.ageTracker.AgeChronologicalYearsFloat,
                            fixedGender: pawn.gender, fixedMelanin: pawn.story.melanin)) as GhostPawn;

                        ghost.Name = pawn.GetGhostName();
                        ghost.Owner = pawn;
                        ghost.DeathLocation = GetPawnDeathLocation(pawn);
                        ghost.GraveLocation = grave.InteractionCell;
                        ghost.story = pawn.story;
                        ghost.apparel = pawn.apparel;

                        GenSpawn.Spawn(ghost, grave.InteractionCell, map);
                        ActiveGhosts.Add(ghost);
                    }
                });
            } else if (map.IsDaytime() && ActiveGhosts.Count > 0) {
                foreach (var ghost in ActiveGhosts.Where(g => g.AtGraveLocation)) {
                    ghost.DeSpawn();
                }
                ActiveGhosts.RemoveAll(g => g.AtGraveLocation);
            }
        }

        public override void ExposeData() {
            var idsOfTheDead = DeathLocations.Select((kvp) => kvp.Key).ToList();
            var locationsOfTheDead = DeathLocations.Select((kvp) => kvp.Value).ToList();

            Scribe_Collections.Look(ref idsOfTheDead, "idsOfTheDead");
            Scribe_Collections.Look(ref locationsOfTheDead, "locationsOfTheDead");

            DeathLocations = idsOfTheDead.Zip(locationsOfTheDead, (s, i) => new KeyValuePair<string, IntVec3>(s, i)).ToDictionary((kvp) => kvp.Key, (kvp) => kvp.Value);
        }
    }
}
