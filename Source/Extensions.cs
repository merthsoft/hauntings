﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Reflection;
using Verse;
using static Merthsoft.Hauntings.TextConstants;
using VersePawn = Verse.Pawn;

namespace Merthsoft.Hauntings {
    public static class Extensions {
        public const float GenCelestial_ShadowDayNightThreshold = 0.6f;
        public const float ShadowAlmostDayNightThreshold = GenCelestial_ShadowDayNightThreshold - 0.2f;

        static readonly BindingFlags fieldFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static | BindingFlags.GetField | BindingFlags.GetProperty;

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<int, T> action) {
            int index = 0;
            foreach (var t in enumerable) {
                action.Invoke(index, t);
            }
        }

        public static T GetInstanceField<T>(this object instance, string fieldName) where T : class {
            var type = instance.GetType();
            var field = type.GetField(fieldName, fieldFlags);
            return field.GetValue(instance) as T;
        }

        public static void SetInstanceField<T>(this object instance, string fieldName, T value) {
            var type = instance.GetType();
            var field = type.GetField(fieldName, fieldFlags);
            field.SetValue(instance, value);
        }

        public static void SetBaseInstanceField<T>(this object instance, string fieldName, T value) {
            var type = instance.GetType().BaseType;
            var field = type.GetField(fieldName, fieldFlags);
            field.SetValue(instance, value);
        }

        public static Name GetGhostName(this VersePawn pawn)
            => new NameSingle(string.Format(GhostNameFormat, pawn.Name.ToStringShort));

        public static IEnumerable<TResult> Zip<TFirst, TSecond, TResult>(this IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> resultSelector) {
            if (first == null) { throw new ArgumentNullException("first"); }
            if (second == null) { throw new ArgumentNullException("second"); }
            if (resultSelector == null) { throw new ArgumentNullException("resultSelector"); }
            return ZipIterator(first, second, resultSelector);
        }

        private static IEnumerable<TResult> ZipIterator<TFirst, TSecond, TResult>(IEnumerable<TFirst> first, IEnumerable<TSecond> second, Func<TFirst, TSecond, TResult> resultSelector) {
            using (IEnumerator<TFirst> e1 = first.GetEnumerator())
            using (IEnumerator<TSecond> e2 = second.GetEnumerator()) {
                while (e1.MoveNext() && e2.MoveNext()) {
                    yield return resultSelector(e1.Current, e2.Current);
                }
            }
        }

        public static bool IsDaytime(this Map map)
            => GenCelestial.CurCelestialSunGlow(map) > GenCelestial_ShadowDayNightThreshold;

        public static bool IsAlmostDaytime(this Map map)
            => GenCelestial.CurCelestialSunGlow(map) > ShadowAlmostDayNightThreshold;
    }
}
