﻿using Harmony;
using System.Linq;
using System.Reflection;
using Verse;

namespace Merthsoft.Hauntings {
    [StaticConstructorOnStartup]
    public class Hauntings : Mod {
        public static HarmonyInstance HarmonyInstance { get; private set; }
        
        public static PawnKindDef GhostPawnKind
            => DefDatabase<PawnKindDef>.AllDefs.FirstOrDefault(pkd => pkd.defName == "Merthsoft_Hauntings_GhostPawnKind");

        static Hauntings() {
            HarmonyInstance = HarmonyInstance.Create("Merthsoft.Hauntings");
            HarmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
        }

        public Hauntings(ModContentPack content) : base(content) {
            Log.Message("Loaded Haunting");
        }
    }
}
