﻿using Harmony;
using Merthsoft.Hauntings.GameComponent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Verse;

namespace Merthsoft.Hauntings.Patches {
    [HarmonyPatch(typeof(Pawn_HealthTracker), "SetDead")]
    public static class Pawn_HealthTracker_SetDead {
        public static void Prefix(Pawn_HealthTracker __instance) {
            var pawn = __instance.GetInstanceField<Verse.Pawn>("pawn");
            var hauntingComponent = pawn.Map.GetComponent<HauntingComponent>();
            hauntingComponent.SetPawnDeathLocation(pawn);
        }
    }
}
