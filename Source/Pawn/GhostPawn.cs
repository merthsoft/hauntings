﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Verse;

namespace Merthsoft.Hauntings.Pawn {
    class GhostPawn : Verse.Pawn {
        public Verse.Pawn Owner { get; set; }
        public IntVec3? DeathLocation { get; set; }
        public IntVec3 GraveLocation { get; set; }
        
        public override Graphic Graphic => Owner.Graphic;

        public bool AtGraveLocation => Position == GraveLocation;

        public override IEnumerable<InspectTabBase> GetInspectTabs() {
            yield break;
        }
    }
}
